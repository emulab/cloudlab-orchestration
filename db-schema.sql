-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: tbbench
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `disk_info`
--

DROP TABLE IF EXISTS `disk_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disk_info` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `disk_name` varchar(20) DEFAULT NULL,
  `disk_model` varchar(32) DEFAULT NULL,
  `disk_serial` varchar(32) DEFAULT NULL,
  `disk_size` varchar(12) DEFAULT NULL,
  `npartitions` tinyint(4) DEFAULT NULL,
  `disk_type` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `disk_results`
--

DROP TABLE IF EXISTS `disk_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disk_results` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `testname` varchar(40) DEFAULT NULL,
  `runtime` float DEFAULT NULL,
  `size` float DEFAULT NULL,
  `max` float DEFAULT NULL,
  `min` float DEFAULT NULL,
  `mean` float DEFAULT NULL,
  `stdev` float DEFAULT NULL,
  `units` varchar(8) DEFAULT NULL,
  `device` varchar(16) DEFAULT NULL,
  `iodepth` smallint(6) DEFAULT NULL,
  UNIQUE KEY `uniq_disk_results` (`run_uuid`,`testname`,`device`,`iodepth`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `env_info`
--

DROP TABLE IF EXISTS `env_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `env_info` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `gcc_ver` varchar(8) DEFAULT NULL,
  `version_hash` varchar(64) DEFAULT NULL,
  `total_mem` varchar(12) DEFAULT NULL,
  `mem_clock_speed` varchar(24) DEFAULT NULL,
  `arch` varchar(8) DEFAULT NULL,
  `run_success` tinyint(1) DEFAULT NULL,
  `site` varchar(16) DEFAULT NULL,
  `nthreads` smallint(6) DEFAULT NULL,
  `nsockets` tinyint(4) DEFAULT NULL,
  `cpu_model` varchar(64) DEFAULT NULL,
  `hw_type` varchar(12) DEFAULT NULL,
  `kernel_release` varchar(24) DEFAULT NULL,
  `os_release` varchar(48) DEFAULT NULL,
  `random` tinyint UNSIGNED DEFAULT NULL,
  UNIQUE KEY `uniq_env_info` (`run_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `extra_npb_cpu_results`
--

DROP TABLE IF EXISTS `extra_npb_cpu_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extra_npb_cpu_results` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `socket_num` tinyint(4) DEFAULT NULL,
  `dvfs` varchar(8) DEFAULT 'yes',
  `run_num` smallint UNSIGNED DEFAULT NULL,
  `testname` char(2) DEFAULT NULL,
  `class` char(1) DEFAULT NULL,
  `size` varchar(16) DEFAULT NULL,
  `iterations` smallint UNSIGNED DEFAULT NULL,
  `exec_time` float DEFAULT NULL,
  `total_threads` tinyint UNSIGNED DEFAULT NULL,
  `avail_threads` tinyint UNSIGNED DEFAULT NULL,
  `mops_total` float DEFAULT NULL,
  `mops_per_thread` float DEFAULT NULL,
  `operation_type` varchar(32) DEFAULT NULL,
  `verification` varchar(13) DEFAULT NULL,
  `version` varchar(12) DEFAULT NULL,
  `compile_date` varchar(16) DEFAULT NULL,
  `compiler` varchar(8) DEFAULT NULL,
  `linker` varchar(8) DEFAULT NULL,
  `lib` varchar(16) DEFAULT NULL,
  `inc` varchar(16) DEFAULT NULL,
  `flags` varchar(16) DEFAULT NULL,
  `linkflags` varchar(16) DEFAULT NULL,
  `rand` varchar(12) DEFAULT NULL,
  UNIQUE KEY `uniq_extra_npb_cpu_results` (`run_uuid`,`run_num`,`testname`,`socket_num`,`dvfs`,`avail_threads`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fio_info`
--

DROP TABLE IF EXISTS `fio_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fio_info` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `fio_version` varchar(12) DEFAULT NULL,
  `fio_size` varchar(8) DEFAULT NULL,
  `fio_iodepth` int(11) DEFAULT NULL,
  `fio_direct` int(11) DEFAULT NULL,
  `fio_numjobs` int(11) DEFAULT NULL,
  `fio_ioengine` varchar(16) DEFAULT NULL,
  `fio_blocksize` varchar(8) DEFAULT NULL,
  `fio_timeout` int(11) DEFAULT NULL,
  UNIQUE KEY `uniq_fio_info` (`run_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `iperf3_info`
--

DROP TABLE IF EXISTS `iperf3_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iperf3_info` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `version` varchar(24) DEFAULT NULL,
  `local_ip` varchar(17) DEFAULT NULL,
  `local_port` smallint(5) unsigned DEFAULT NULL,
  `remote_ip` varchar(17) DEFAULT NULL,
  `remote_port` smallint(5) unsigned DEFAULT NULL,
  `remote_nodeid` varchar(20) DEFAULT NULL,
  `protocol` varchar(6) DEFAULT NULL,
  `num_streams` tinyint(2) DEFAULT NULL,
  `buffer_size` mediumint(9) DEFAULT NULL,
  `omitted_intervals` tinyint(4) DEFAULT NULL,
  `duration` smallint(6) DEFAULT NULL,
  `time_units` varchar(8) DEFAULT NULL,
  UNIQUE KEY `uniq_iperf3_info` (`run_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `iperf3_results`
--

DROP TABLE IF EXISTS `iperf3_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iperf3_results` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `reverse` tinyint(4) DEFAULT NULL,
  `retransmits` mediumint(9) DEFAULT NULL,
  `local_cpu_util` float DEFAULT NULL,
  `remote_cpu_util` float DEFAULT NULL,
  `median` bigint(20) DEFAULT NULL,
  `max` bigint(20) DEFAULT NULL,
  `min` bigint(20) DEFAULT NULL,
  `mean` float DEFAULT NULL,
  `stdev` float DEFAULT NULL,
  `sum_sent` bigint(20) DEFAULT NULL,
  `sum_received` bigint(20) DEFAULT NULL,
  `units` varchar(8) DEFAULT NULL,
  UNIQUE KEY `uniq_iperf3_results` (`run_uuid`,`reverse`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mem_results`
--

DROP TABLE IF EXISTS `mem_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mem_results` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `nthreads_used` int(11) DEFAULT NULL,
  `testname` varchar(40) DEFAULT NULL,
  `max` float DEFAULT NULL,
  `min` float DEFAULT NULL,
  `mean` float DEFAULT NULL,
  `stdev` float DEFAULT NULL,
  `units` varchar(8) DEFAULT NULL,
  `socket_num` tinyint(4) DEFAULT NULL,
  `dvfs` varchar(8) DEFAULT 'yes',
  UNIQUE KEY `uniq_mem_results` (`run_uuid`,`testname`,`socket_num`,`dvfs`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `membench_info`
--

DROP TABLE IF EXISTS `membench_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `membench_info` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `membench_size` varchar(16) DEFAULT NULL,
  `membench_samples` int(11) DEFAULT NULL,
  `membench_times` int(11) DEFAULT NULL,
  `membench_optimization` char(2) DEFAULT NULL,
  UNIQUE KEY `uniq_membench_info` (`run_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `npb_cpu_results`
--

DROP TABLE IF EXISTS `npb_cpu_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `npb_cpu_results` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `socket_num` tinyint(4) DEFAULT NULL,
  `dvfs` varchar(8) DEFAULT 'yes',
  `testname` char(2) DEFAULT NULL,
  `class` char(1) DEFAULT NULL,
  `size` varchar(16) DEFAULT NULL,
  `iterations` smallint UNSIGNED DEFAULT NULL,
  `exec_time` float DEFAULT NULL,
  `total_threads` tinyint UNSIGNED DEFAULT NULL,
  `avail_threads` tinyint UNSIGNED DEFAULT NULL,
  `mops_total` float DEFAULT NULL,
  `mops_per_thread` float DEFAULT NULL,
  `operation_type` varchar(32) DEFAULT NULL,
  `verification` varchar(13) DEFAULT NULL,
  `version` varchar(12) DEFAULT NULL,
  `compile_date` varchar(16) DEFAULT NULL,
  `compiler` varchar(8) DEFAULT NULL,
  `linker` varchar(8) DEFAULT NULL,
  `lib` varchar(16) DEFAULT NULL,
  `inc` varchar(16) DEFAULT NULL,
  `flags` varchar(16) DEFAULT NULL,
  `linkflags` varchar(16) DEFAULT NULL,
  `rand` varchar(12) DEFAULT NULL,
  UNIQUE KEY `uniq_npb_cpu_results` (`run_uuid`,`testname`,`socket_num`,`dvfs`,`avail_threads`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `network_info`
--

DROP TABLE IF EXISTS `network_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `network_info` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `vlan_name` varchar(12) DEFAULT NULL,
  `vlan_ip` varchar(15) DEFAULT NULL,
  `vlan_hwaddr` varchar(17) DEFAULT NULL,
  `vlan_driver` varchar(24) DEFAULT NULL,
  `vlan_driver_ver` varchar(16) DEFAULT NULL,
  `if_name` varchar(12) DEFAULT NULL,
  `if_hwaddr` varchar(17) DEFAULT NULL,
  `if_hwinfo` varchar(64) DEFAULT NULL,
  `if_speed` varchar(12) DEFAULT NULL,
  `if_duplex` varchar(8) DEFAULT NULL,
  `if_port_type` varchar(32) DEFAULT NULL,
  `if_driver` varchar(24) DEFAULT NULL,
  `if_driver_ver` varchar(16) DEFAULT NULL,
  `if_bus_location` varchar(16) DEFAULT NULL,
  `switch_path` varchar(256) DEFAULT NULL,
  UNIQUE KEY `uniq_network_info` (`run_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ping_info`
--

DROP TABLE IF EXISTS `ping_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ping_info` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `ping_version` varchar(24) DEFAULT NULL,
  `ping_count` varchar(12) DEFAULT NULL,
  `ping_source_ip` varchar(17) DEFAULT NULL,
  `ping_dest_ip` varchar(17) DEFAULT NULL,
  `ping_dest_nodeid` varchar(20) DEFAULT NULL,
  `ping_size` varchar(8) DEFAULT NULL,
  UNIQUE KEY `uniq_ping_info` (`run_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ping_results`
--

DROP TABLE IF EXISTS `ping_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ping_results` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `runtime` varchar(16) DEFAULT NULL,
  `packets_sent` int(12) DEFAULT NULL,
  `packets_received` int(12) DEFAULT NULL,
  `packet_loss` varchar(6) DEFAULT NULL,
  `max` float DEFAULT NULL,
  `min` float DEFAULT NULL,
  `mean` float DEFAULT NULL,
  `stdev` float DEFAULT NULL,
  `ipg` float DEFAULT NULL,
  `ewma` float DEFAULT NULL,
  `units` varchar(8) DEFAULT NULL,
  UNIQUE KEY `uniq_ping_results` (`run_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `probed_info`
--

DROP TABLE IF EXISTS `probed_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `probed_info` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `probed_version` varchar(24) DEFAULT NULL,
  `probed_count` varchar(12) DEFAULT NULL,
  `probed_source_ip` varchar(17) DEFAULT NULL,
  `probed_dest_ip` varchar(17) DEFAULT NULL,
  `probed_dest_nodeid` varchar(20) DEFAULT NULL,
  `probed_interval` varchar(8) DEFAULT NULL,
  UNIQUE KEY `uniq_probed_info` (`run_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `probed_results`
--

DROP TABLE IF EXISTS `probed_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `probed_results` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `rtt_max` mediumint UNSIGNED DEFAULT NULL,
  `rtt_min` mediumint UNSIGNED DEFAULT NULL,
  `rtt_mean` float DEFAULT NULL,
  `rtt_stdev` float DEFAULT NULL,
  `total_rtt_max` mediumint UNSIGNED DEFAULT NULL,
  `total_rtt_min` mediumint UNSIGNED DEFAULT NULL,
  `total_rtt_mean` float DEFAULT NULL,
  `total_rtt_stdev` float DEFAULT NULL,
  `oh_max` mediumint UNSIGNED DEFAULT NULL,
  `oh_min` mediumint UNSIGNED DEFAULT NULL,
  `oh_mean` float DEFAULT NULL,
  `oh_stdev` float DEFAULT NULL,
  `ok_reqs` smallint UNSIGNED DEFAULT NULL,
  `dscp_errors` smallint UNSIGNED DEFAULT NULL,
  `ts_errors` smallint UNSIGNED DEFAULT NULL,
  `unknown_or_dups` smallint UNSIGNED DEFAULT NULL,
  `lost_pongs` smallint UNSIGNED DEFAULT NULL,
  `timeouts` smallint UNSIGNED DEFAULT NULL,
  `pct_loss` float DEFAULT NULL,
  `thold` mediumint UNSIGNED DEFAULT NULL,
  `pct_over_thold` float DEFAULT NULL,
  `units` varchar(8) DEFAULT NULL,
  UNIQUE KEY `uniq_probed_results` (`run_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `random_info`
--

DROP TABLE IF EXISTS `random_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `random_info` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `random_indices` text,
  `random_ops` text,
  `random_status` text,
  UNIQUE KEY `uniq_random_info` (`run_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stream_info`
--

DROP TABLE IF EXISTS `stream_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stream_info` (
  `run_uuid` char(36) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `nodeid` varchar(20) DEFAULT NULL,
  `nodeuuid` char(36) DEFAULT NULL,
  `stream_type` varchar(12) DEFAULT NULL,
  `stream_array_size` int(11) DEFAULT NULL,
  `stream_offset` int(11) DEFAULT NULL,
  `stream_ntimes` int(11) DEFAULT NULL,
  `stream_optimization` char(2) DEFAULT NULL,
  UNIQUE KEY `uniq_stream_info` (`run_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;