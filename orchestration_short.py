# Import geni alone to get error definitions
import geni

# Context functions
from geni.aggregate import FrameworkRegistry
from geni.aggregate.context import Context
from geni.aggregate.user import User

# Cloudlab and request libraries
import geni.aggregate.cloudlab as cloudlab
import geni.aggregate.protogeni as emulabPG
import geni.aggregate.apt as apt
import geni.rspec.pg as PG
import geni.rspec.emulab as emulab
import geni.urn as urn
import geni.util as geniutil

# Geni-lib config
import geni.minigcf.config as geniconf

# Python SSH library
import paramiko

# For simple HTTP requests
import requests

# Subprocess functions
from subprocess import Popen,PIPE,STDOUT,call

# Libraries for SQL operations, parsing, and formatting
import pymysql
import csv
import json
import xml.etree.ElementTree as ET

# System libraries
import os
import fnmatch
import sys
import argparse

# Multithreading library
import threading

# Time libraries and RNG
from random import choice as randomchoice
from time import sleep,time
import datetime

# Math library
import numpy as np

# Exceptions and traceback
import requests.exceptions
import traceback

##############################
### Define sshThread class ###
##############################
class sshThread(threading.Thread):
    def __init__(self, threadID, threadName, server, portnum, uname, keyfile, dest_dir, sliceexp, results):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.threadName = threadName
        self.server = server
        self.portnum = portnum
        self.uname = uname
        self.keyfile = keyfile
        self.dest_dir = dest_dir
        self.sliceexp = sliceexp
        self.results = results
    def run(self):
        print "Starting thread " + self.threadName + " (" + self.server + ") - " + str(datetime.datetime.today())
        result = runRemoteExperiment(self.server, self.portnum, self.uname, self.keyfile, self.dest_dir, self.sliceexp)
        self.results[self.threadID] = result
        print "Exiting thread " + self.threadName + " (" + self.server + ") - " + str(datetime.datetime.today())

#######################
### Parse Arguments ###
#######################
def parseArgs():
    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--nslices", action="store", dest="nslices", type=int, default=1, help="Number of slices to allocate")
    parser.add_argument("-s", "--site", action="store", dest="site", type=str, default="", help="Site to allocate nodes from")
    parser.add_argument("-c", "--cert", action="store", dest="cert_path", type=str, default="", help="Path to framework cert/key bundle")
    parser.add_argument("-k", "--ssh_priv_key", action="store", dest="priv_key_path", type=str, default="", help="Path to ssh private key")
    parser.add_argument("-b", "--ssh_pub_key", action="store", dest="pub_key_path", type=str, default="", help="Path to ssh private key")
    parser.add_argument("-u", "--user", action="store", dest="user", type=str, default="", help="User for geni-lib context")
    parser.add_argument("-p", "--project", action="store", dest="project", type=str, default="", help="Project for geni-lib context")
    parser.add_argument("-d", "--dest", action="store", dest="dest_dir", type=str, default="", help="Destination directory for results")
    parser.add_argument("-g", "--geni_cache", action="store", dest="geni_cache_path", type=str, default="", help="(Optional) Geni-lib cache directory")
    
    args = parser.parse_args()

    if any(arg == "" for arg in (args.site, args.cert_path, args.priv_key_path, \
                                 args.pub_key_path, args.user, args.project, args.dest_dir)):
        print "Invalid Arguments"
        parser.print_help(sys.stderr)
        sys.exit(1)
        
    return args

#######################
### Build a context ###
#######################
def buildContext(cert_path, pub_key_path, username, project):
    # Set up Framework
    framework = FrameworkRegistry.get("emulab-ch2")()
    framework.cert = cert_path
    framework.key = cert_path

    # Set up User
    user = User()
    user.name = username
    user.urn = "urn:publicid:IDN+emulab.net+user+" + username
    user.addKey(pub_key_path)

    # Combine Framework and User into a full context
    context = Context()
    context.addUser(user)
    context.cf = framework
    context.project = project

    return context

####################################
### Get a list of all free nodes ###
####################################
def getFreeNodes(context, site):
    nfailures = 0
    maxfailures = 3
    print "Querying AM for list of resources at site " + site + "."
    while True:
        try:
            if site == "utah" or site == "utah_d6515":
                ad = cloudlab.Utah.listresources(context, available = True)
            elif site == "wisc":
                ad = cloudlab.Wisconsin.listresources(context, available = True)
            elif site == "clemson":
                ad = cloudlab.Clemson.listresources(context, available = True)
            elif site == "apt":
                ad = apt.Apt.listresources(context, available = True)
            elif site == "emulab":
                ad = emulabPG.UTAH_PG.listresources(context, available = True)
            else:
                print "\tInvalid Site, exiting."
                sys.exit(2)
        except Exception as e:
            nfailures += 1
            print "In getFreeNodes: " + repr(e) + " - " + str(e)
            if nfailures >= maxfailures:
                print "\tToo many errors, exiting."
                sys.exit(2)
            else:
                print "\tRetrying listresources..."
        else:
            break
    # Check each node to make sure it contains one of these hardware types
    # Contains each tested hardware type across all sites
    # Only use hardware types that there are enough nodes for
    nodes = []
    hw_types = ['m510', 'm400', 'xl170', 'c220g1', 'c220g2', 'c220g5', \
               'c8220', 'c6320', 'c6420', 'r320', 'c6220', 'd430']
    if site == "utah_d6515":
        hw_types = ['d6515']
    print "Initial free node list:  " + str(len(ad.nodes))
    for node in ad.nodes:
        if node.available == True:
            hw_type = set(hw_types).intersection(node.hardware_types)
            if len(hw_type) == 1:
                hw_type = list(hw_type)[0]
                node.hw_type = hw_type
                nodes.append(node)
            elif len(hw_type) > 1:
                print "Unexpected number of matching hardware types for " + \
                "node " + node.name + ", skipping node..."
    print "Final free node list:  " + str(len(nodes))
    return nodes

#######################################################################
### Get a list of size nslices of nodes to test from free node pool ###
#######################################################################
def chooseTestNodes(context, nodes, nslices, site, hwtypes):
    # Set up prelim data structures and SQL variables
    untestednodes = []
    testednodes = []
    most_recent = {}
    testnodes = []
    server = 'localhost'
    user = 'tbbench_user'
    password = 'tbbench_pass'
    database = 'tbbench'

    print "Choosing test nodes from free node list..."

    # Attempt SQL connection, give it a few times before failing
    nTries = 0
    maxTries = 3
    while True:
        try:
            conn = pymysql.connect(host = server, user = user, passwd = password, db = database)
        except Exception as e:
            nTries += 1
            print "In insertResults: " + repr(e) + " - " + str(e)
            print "Error #" + str(nTries) + " out of " + str(maxTries) + "."
            if nTries >= maxTries:
                print "\tCould not connect to DB, exiting..."
                sys.exit(2)
            else:
                sleep(10)
                print "\tRetrying..."
        else:
            cur = conn.cursor()
            break

    # The first SQL query, getting distinct list of previously tested nodes, placing in appropriate lists
    cur.execute("SELECT DISTINCT nodeid FROM env_info WHERE site = " + conn.escape(site) + ";");
    sql_results = cur.fetchall()
    sql_results = [item for tpl in sql_results for item in tpl]
    print "Selecting based on hardware types:  " + str(hwtypes)
    for node in nodes:
        if node.hw_type in hwtypes:
            if node.name in sql_results:
                testednodes.append(node)
            else:
                untestednodes.append(node)

    print "Tested nodes count:  " + str(len([node.name for node in testednodes]))
    print "Untested nodes count:  " + str(len([node.name for node in untestednodes]))
    # For each tested node, check when the last time it was tested was.
    for node in testednodes:
        sql = "SELECT timestamp, run_success FROM env_info WHERE nodeid = " + conn.escape(node.name) + " ORDER BY timestamp DESC"
        cur = conn.cursor(pymysql.cursors.DictCursor)
        cur.execute(sql)
        result = cur.fetchone()
        # If the last time it ran it was unsuccessful, don't add unless it's been a week.
        if result['run_success'] > 0:
            most_recent[node] = result
        else:
            currenttime = int(time())
            if currenttime - int(result['timestamp']) > 604800:
                most_recent[node] = result

    # Fill in the list of test nodes, prioritizing untested nodes
    while len(testnodes) < nslices:
        if len(untestednodes) > 0:
            node = randomchoice(untestednodes)
            testnodes.append([node.name, node.hw_type])
            nodes.remove(node)
            untestednodes.remove(node)
        else:
            if len(most_recent) > 0:
                last_timestamp = min(int(d['timestamp']) for d in most_recent.values())
                last_node = [k for k in most_recent if int(most_recent[k]['timestamp']) == last_timestamp][0]
                testnodes.append([last_node.name, last_node.hw_type])
                del most_recent[last_node]
                nodes.remove(last_node)
            else:
                break
    cur.close()
    conn.close()
    print "Test Nodes chosen: " + str(testnodes)
    return testnodes

##########################
### Create a new Slice ###
##########################
def createSlice(context, slicename, sliceexp):
    try:
        # Attempt to create Slice with slicename and sliceexp
        context.cf.createSlice(context, slicename, exp = sliceexp)
    # In the event of any exception, consider slice unusable, return error.
    except Exception as e:
        print "In createSlice: " + repr(e) + " - " + str(e)
        print "\tDiscarding Slice " + slicename + "."
        return 1
    # Otherwise, return success.
    else:
        return 0

###########################
### Create a new Sliver ###
###########################
def createSliver(context, testnode, slicename, nodes, hwtypes, site, slicenum):
    # Set up initial vars
    choosenewnode = False
    nfailures = 0
    maxfailures = 3
    testnode_ret = testnode
    test_hwtypes = hwtypes
    print "Initial testnode_ret: " + str(testnode_ret)
    
    # Main loop (So we can retry on certain failures)
    while True:
        if choosenewnode == True:
            newnode = chooseTestNodes(context, nodes, 1, site, test_hwtypes)
            if len(newnode) == 0:
                print "\tNo nodes available to run experiments on.  Ignoring Slice."
                return "IGNORESLICE"
            testnode_ret = newnode[0]
            print "Choose New Node:  " + str(testnode_ret)
            choosenewnode = False
        # Create a Request object to start building the RSpec.
        rspec = PG.Request()
        
        # Add a raw PC to the request, and create sliver
        node = PG.RawPC(testnode_ret[0])
        if site == "utah":
            node.disk_image = urn.Image(cloudlab.Utah, "emulab-ops:UBUNTU18-64-STD")
            node.component_id = urn.Node(cloudlab.Utah, testnode_ret[0])
        elif site == "wisc":
            node.disk_image = urn.Image(cloudlab.Wisconsin, "emulab-ops:UBUNTU18-64-STD")
            node.component_id = urn.Node(cloudlab.Wisconsin, testnode_ret[0])
        elif site == "clemson":
            node.disk_image = urn.Image(cloudlab.Clemson, "emulab-ops:UBUNTU18-64-STD")
            node.component_id = urn.Node(cloudlab.Clemson, testnode_ret[0])
        elif site == "apt":
            node.disk_image = urn.Image(apt.Apt, "emulab-ops:UBUNTU18-64-STD")
            node.component_id = urn.Node(apt.Apt, testnode_ret[0])        
        elif site == "emulab":
            node.disk_image = urn.Image(emulabPG.UTAH_PG, "emulab-ops:UBUNTU18-64-STD")
            node.component_id = urn.Node(emulabPG.UTAH_PG, testnode_ret[0])
                        
        iface = node.addInterface()
        # Specify interface bandwidths in Kbps for d430s
        ipaddr = "192.168.1." + str(slicenum)
        iface.addAddress(PG.IPv4Address(ipaddr, "255.255.255.0"))
        link = rspec.Link("link")
        link.addInterface(iface)
        if testnode_ret[1] == 'xl170':
            link.setProperties(bandwidth=25000000)
        elif testnode_ret[1] == 'd710':
            link.setProperties(bandwidth=1000000)
        else:
            link.setProperties(10000000)
        link.connectSharedVlan("cloudlab-benchmarks")
        
        rspec.addResource(node)
        while True:
            # Attempt to create a new sliver.  If this fails too many times,
            # then ignore slice and move on.
            try:
                if site == "utah":
                    ad = cloudlab.Utah.createsliver(context, slicename, rspec)
                elif site == "wisc":
                    ad = cloudlab.Wisconsin.createsliver(context, slicename, rspec)
                elif site == "clemson":
                    ad = cloudlab.Clemson.createsliver(context, slicename, rspec)
                elif site == "apt":
                    ad = apt.Apt.createsliver(context, slicename, rspec)
                elif site == "emulab":
                    ad = emulabPG.UTAH_PG.createsliver(context, slicename, rspec)
                    
            except geni.aggregate.pgutil.InsufficientNodesError as e:
                # InsufficientNodesError tends to mean that even though nodes of a
                # specific hardware type are "free", there are reservations for
                # nodes of that hardware type that prevent us from getting them.
                # Pick another node of a different hardware type and try again.
                print "Error - (" + testnode_ret[0] + ")."
                print "In createSliver: " + repr(e) + " - " + str(e)
                if testnode_ret[1] in test_hwtypes:
                    test_hwtypes.remove(testnode_ret[1])
                else:
                    print "\tHardware Type " + str(testnode_ret[1]) + " already removed from available types."
                if len(test_hwtypes) > 0:
                    print "\tChoosing new node and trying again."
                    choosenewnode = True
                    break
                else:
                    print "\tNo non-reserved hardware types available, discarding slice."
                    return "IGNORESLICE"
            except geni.aggregate.pgutil.NoMappingError as e:
                # NoMappingError usually means specific node can't be used.
                # Pick another node and try again.
                nfailures += 1
                print "Error #" + str(nfailures) + " out of " + str(maxfailures) + " - (" + testnode_ret[0] + ")."
                print "In createSliver: " + repr(e) + " - " + str(e)
                if nfailures < maxfailures:
                    print "\tChoosing new node and trying again."
                    choosenewnode = True
                    break
                else:
                    print "\tToo many errors, discarding slice."
                    return "IGNORESLICE"
            except geni.aggregate.frameworks.ClearinghouseError as e:
                # Usually a result of "Slice is busy".  Sleep, and try again.
                nfailures += 1
                print "Error #" + str(nfailures) + " out of " + str(maxfailures) + "."
                print "In createSliver: " + repr(e) + " - " + str(e)
                if nfailures < maxfailures:
                    print "\tSleeping, then trying again."
                    sleep(30)
                else:
                    print "\tToo many errors, discarding slice."
                    return "IGNORESLICE"
            except requests.exceptions.ReadTimeout as e:
                # Occasionally the connection to the aggregate manager times out
                # Sleep, and try again.
                nfailures += 1
                print "Error #" + str(nfailures) + " out of " + str(maxfailures) + "."
                print "In createSliver: " + repr(e) + " - " + str(e)
                if nfailures < maxfailures:
                    print "\tSleeping, then trying again."
                    sleep(60)
                    # Sometimes ReadTimeout occurs even if AM would otherwise report
                    # a successful sliver creation.  Attempt to check this case.
                    print "\tGetting Sliver status to check if creation went through anyways..."
                    try:
                        if site == "utah":
                            res = cloudlab.Utah.sliverstatus(context, slicename)
                        elif site == "wisc":
                            res = cloudlab.Wisconsin.sliverstatus(context, slicename)
                        elif site == "clemson":
                            res = cloudlab.Clemson.sliverstatus(context, slicename)
                        elif site == "apt":
                            res = apt.Apt.sliverstatus(context, slicename)
                        elif site == "emulab":
                            res = emulabPG.UTAH_PG.sliverstatus(context, slicename)
                    except Exception as ee:
                        print "\tIn createSliver: " + repr(ee) + " - " + str(ee)
                        print "\t\tException when checking sliver status, try again"
                    else:
                        # If sliver status doesn't throw an exception, we're probably fine
                        print "\tSliver status successful, assume sliver was created"
                        return testnode_ret
                else:
                    print "\tToo many errors, discarding slice."
                    return "IGNORESLICE"
            except Exception as e:
                # Any other exceptions, fail immediately.
                # One exception type that gets raised doesn't like str(e)
                # Using only traceback.print_exc(e) to be safe
                print traceback.print_exc(e)
                print "\tUnhandled exception, discarding slice."
                return "IGNORESLICE"
            else:
                # Otherwise, return nodename.
                print "END OF createSliver: " + str(testnode_ret)
                return testnode_ret

    # We shouldn't get here, but return error just in case
    print "Reached end of function without returning.  This is not supposed to happen, discarding slice."
    return "IGNORESLICE"
    
##################################
### Delete a Sliver on a Slice ###
##################################
def deleteSliver(context, slicename, site):
    nfailures = 0
    maxfailures = 3
    print "Attempting to delete sliver on slice " + slicename + "..."
    while True:
        try:
            if site == "utah":
                cloudlab.Utah.deletesliver(context, slicename)
            elif site == "wisc":
                cloudlab.Wisconsin.deletesliver(context, slicename)
            elif site == "clemson":
                cloudlab.Clemson.deletesliver(context, slicename)
            elif site == "apt":
                apt.Apt.deletesliver(context, slicename)
            elif site == "emulab":
                emulabPG.UTAH_PG.deletesliver(context, slicename)
            else:
                print "\tInvalid Site, exiting."
                sys.exit(2)          
        except Exception as e:
            nfailures += 1
            print "In deleteSliver: " + repr(e) + " - " + str(e)
            if nfailures >= maxfailures:
                print "\tToo many errors, letting sliver expire naturally."
                break
            else:
                sleep(10)
                print "\tRetrying sliver deletion..."
        else:
            break

#####################################################
### Run experiments on remote host using Paramiko ###
#####################################################
def runRemoteExperiment(server, portnum, uname, keyfile, dest_dir, sliceexp):
    ssh = paramiko.SSHClient()
    sshkey = paramiko.RSAKey.from_private_key_file(keyfile)
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    # Spin until the machine comes up and is ready for SSH
    nTries = 0
    maxTries = 8
    print "Awaiting completion of provisioning for " + server + ", sleeping for 4 minutes..."
    sleep(240)
    while True:
        try:
            out = Popen(["nc", "-z", "-v", "-w5", server, "22"],stderr=STDOUT,stdout=PIPE)
        except:
            print "In runRemoteExperiment: " + repr(e) + " - " + str(e)
        else:
            t = out.communicate()[0],out.returncode
            if t[1] == 0:
                break
            else:
                nTries += 1
                if nTries > maxTries:
                    return "Failure"
                else:
                    print "\tConnection attempt to " + server + " timed out, retrying (" + str(nTries) + " out of " + str(maxTries) + ")..."
                    sleep(60)

    print "Node " + server + " is up at " + str(datetime.datetime.today()) + ", connecting via SSH."

    # SSH connect, open stdin and stdout channels
    nTries = 0
    maxTries = 3
    while True:
        try:
            ssh.connect(hostname = server, port = portnum, username = uname, pkey = sshkey)
        except Exception as e:
            nTries += 1
            print "In runRemoteExperiment: " + repr(e) + " - " + str(e)
            print "Error #" + str(nTries) + " out of " + str(maxTries) + "."
            if nTries >= maxTries:
                return "Failure"
            else:
                sleep(10)
                print "\tRetrying..."
        else:
            print "SSH connection to " + server + " successful."
            try:
                transport = ssh.get_transport()
                channel = transport.open_session()
                rand = randomchoice(range(0,100))
                if rand < 50:
                    channel.exec_command('/bin/bash -c \
                        "cd /users/' + uname + '/ && \
                        git clone https://gitlab.flux.utah.edu/emulab/cloudlab-benchmarks.git && \
                        nohup /usr/bin/python3 /users/' + uname + '/cloudlab-benchmarks/run_benchmarks_short.py > \
                            /users/' + uname + '/out.log 2> /users/' + uname + '/out_error.log \&"')
                else:
                    channel.exec_command('/bin/bash -c \
                        "cd /users/' + uname + '/ && \
                        git clone https://gitlab.flux.utah.edu/emulab/cloudlab-benchmarks.git && \
                        nohup /users/' + uname + '/cloudlab-benchmarks/run_benchmarks_short.sh > \
                            /users/' + uname + '/out.log 2> /users/' + uname + '/out_error.log \&"')
            except Exception as e:
                print "In runRemoteExperiment: " + repr(e) + " - " + str(e)
                return "Failure"
            else:
                break
    
    # Check if we're done yet periodically.
    nTries = 0
    maxTries = 3
    while True:
        sleep(300)
        try:
            is_complete = requests.get('http://' + str(server) + ':8000/run_complete')
        except Exception as e:
            nTries += 1
            print "In runRemoteExperiment: " + repr(e) + " - " + str(e)
            print "Error #" + str(nTries) + " out of " + str(maxTries) + "."
            if nTries >= maxTries:
                return "Failure"
        else:
            # This will return "INCOMPLETE" until the script is done.
            if (is_complete.status_code == 200):
                now = datetime.datetime.today()
                utcnow = datetime.datetime.utcnow()
                if (is_complete.text == "COMPLETED\n"):
                    print str(server) + " - Benchmarking completed - " + str(now)
                    channel.close()
                    ssh.close()
                    break
                else:
                    time_left = sliceexp - utcnow
                    # If we're too close to slice expiration with still no results, assume failure.
                    if (time_left.seconds < 480):
                        return "Failure"
                    print str(server) + " - Benchmarking not finished - " + str(now)
            else:
                nTries += 1
                print "In runRemoteExperiment: Status check failed..."
                print "Error #" + str(nTries) + " out of " + str(maxTries) + "."
                if nTries >= maxTries:
                    return "Failure"
        
    
    # Make directory for results if it doesn't already exist, then sftp the results over.
    nTries = 0
    maxTries = 3
    while True:
        try:
            # Make directory if it doesn't exist, then erase files if any exist
            # so we don't attempt to import old files
            call(["mkdir", dest_dir], shell = False)
            files = os.listdir(dest_dir)
            for file in files:
                os.remove(os.path.join(dest_dir,file))
            call(["sftp", "-oStrictHostKeyChecking=no", "-P", str(portnum), "-i", keyfile, uname + "@" + server + ":/users/" + uname + "/*.*", dest_dir], shell = False)
        except Exception as e:
            nTries += 1
            print "In runRemoteExperiment: " + repr(e) + " - " + str(e)
            print "Error #" + str(nTries) + " out of " + str(maxTries) + "."
            if nTries >= maxTries:
                return "Failure"
            else:
                print "\tRetrying..."
        else:
            break
    return "Success"

#####################################
### Get Switch Path from Manifest ###
#####################################
def getSwitchPath(context, slicename, site):
    nfailures = 0
    maxfailures = 3
    print "Attempting to get switch path on slice " + slicename + "..."
    while True:
        try:
            if site == "utah":
                res = cloudlab.Utah.listresources(context, slicename)
            elif site == "wisc":
                res = cloudlab.Wisconsin.listresources(context, slicename)
            elif site == "clemson":
                res = cloudlab.Clemson.listresources(context, slicename)
            elif site == "apt":
                res = apt.Apt.listresources(context, slicename)
            elif site == "emulab":
                res = emulabPG.UTAH_PG.listresources(context, slicename)
            else:
                print "\tInvalid Site, exiting."
                sys.exit(2)          
        except Exception as e:
            nfailures += 1
            print "In getSwitchPath: " + repr(e) + " - " + str(e)
            if nfailures >= maxfailures:
                print "\tToo many errors, aborting switch path fetch..."
                return "N/A"
            else:
                sleep(10)
                print "\tRetrying manifest fetch..."
        else:
            break
    
    try:
        root = ET.fromstring(res.text)
        link = root.find('{http://www.geni.net/resources/rspec/3}link')
        switchpath = link.find('{http://www.protogeni.net/resources/rspec/ext/emulab/1}switchpath')
    except Exception as e:
        print "In getSwitchPath: " + repr(e) + " - " + str(e)
        print "/tCould not parse Manifest, aborting switch path fetch"
        return "N/A"
    else:
        print "\t" + str(switchpath.text)
        return switchpath.text
            

###############################################
### Insert experiment results into database ###
###############################################
def insertResults(results_dir, hw_type, site, switchpath, nodename):
    print "Inserting Results into SQL database for node - " + str(nodename)

    # Set up SQL connection vars
    server = 'localhost'
    user = 'tbbench_user'
    password = 'tbbench_pass'
    database = 'tbbench'
    
    # Try to connect to the DB.  Give it a few tries before failing
    nTries = 0
    maxTries = 3
    while True:
        try:
            conn = pymysql.connect(host = server, user = user, passwd = password, db = database)
        except Exception as e:
            nTries += 1
            print "In insertResults (pymysql.connect): " + repr(e) + " - " + str(e)
            print "Error #" + str(nTries) + " out of " + str(maxTries) + "."
            if nTries >= maxTries:
                print "\tCould not connect to DB, exiting..."
                sys.exit(2)
            else:
                sleep(10)
                print "\tRetrying..."
        else:
            cur = conn.cursor()
            break
            
    ###########
    ### ENV ###
    ###########
    # Open file, save to dict
    path = results_dir + "/env_out.csv"
    envdict = {}
    try:
        with open(path) as csvfile:
            reader = csv.DictReader(csvfile)
            envdict = list(reader)[0]
    except Exception as e:
        # This is the one file we want to treat as non-negotiable.
        # If this fails, the run should be listed as failed.
        print "In insertResults (env info open): " + repr(e) + " - " + str(e)
        cur.close()
        conn.close()
        insertFailure(nodename, site)
        return "Failure"
    else:
        csvfile.close()

    # Pull out values into other variables for later use
    timestamp = envdict["timestamp"]
    nodeid = envdict["nodeid"]
    nodeuuid = envdict["nodeuuid"]
    run_uuid = envdict["run_uuid"]
    is_random = envdict["random"]

    # Set table, escape values, setup SQL command and execute
    envdict["run_success"] = 1
    envdict["site"] = site
    envdict["hw_type"] = hw_type 
    # Move rest of env to the end so we can properly update if a run was "incomplete"
    
    ##############
    ### RANDOM ###
    ##############
    if is_random == "1":
        # Open file, save to dict
        results_file = "random_info.csv"
        path = results_dir + "/" + results_file
        randomdict = {}
        try:
            with open(path) as csvfile:
                reader = csv.DictReader(csvfile, delimiter=";")
                randomdict = list(reader)[0]
        except Exception as e:
            print "In insertResults (random info open): " + repr(e) + " - " + str(e)
            print "\tFile: " + str(results_file)
            print "Submitting as an Incomplete Run"
            # Mark this as an incomplete run
            envdict["run_success"] = 2
        else:
            csvfile.close()
            table = 'random_info'
            for k, v in randomdict.iteritems():
                randomdict[k] = conn.escape(v)
            cols = randomdict.keys()
            vals = randomdict.values()
            try:
                sql = "INSERT INTO %s(%s) VALUES(%s);" % (table, ",".join(cols), ",".join(vals))
                cur.execute(sql)
            except Exception as e:
                print "In insertResults (random info execute): " + repr(e) + " - " + str(e)
                print "\tFile: " + str(results_file)
                print "Submitting as an Incomplete Run"
                # Mark this as an incomplete run
                envdict["run_success"] = 2
        
    
    #################
    ### DISK INFO ###
    #################   
    filelist = []
    for results_file in os.listdir(results_dir):
        if fnmatch.fnmatch(results_file, 'disk_info_*'):
            filelist.append(results_file)
        
    for results_file in filelist:
        # Open file, save to dict
        diskdict = {}
        path = results_dir + "/" + results_file
        try:
            with open(path) as csvfile:
                reader = csv.DictReader(csvfile)
                diskdict = list(reader)[0]
        except Exception as e:
            print "In insertResults (disk info open): " + repr(e) + " - " + str(e)
            print "\tFile: " + str(results_file)
            print "Submitting as an Incomplete Run"
            # Mark this as an incomplete run
            envdict["run_success"] = 2
        else:
            csvfile.close()
            # Set table, escape values, setup SQL command and execute
            table = 'disk_info'
            for k, v in diskdict.iteritems():
                diskdict[k] = conn.escape(v)
            cols = diskdict.keys()
            vals = diskdict.values()
            try:
                sql = "INSERT INTO %s(%s) VALUES(%s);" % (table, ",".join(cols), ",".join(vals))
                cur.execute(sql)
            except Exception as e:
                print "In insertResults (disk info execute): " + repr(e) + " - " + str(e)
                print "\tFile: " + str(results_file)
                print "Submitting as an Incomplete Run"
                # Mark this as an incomplete run
                envdict["run_success"] = 2
        
    #################
    ### MEM TESTS ###
    #################
    memtests = ['stream', 'membench']
    for test in memtests:
        # Open file, save to dict
        memdict = {}
        path = results_dir + "/" + test + "_info.csv"
        try:
            with open(path) as csvfile:
                reader = csv.DictReader(csvfile)
                memdict = list(reader)[0]
        except Exception as e:
            print "In insertResults (mem info open): " + repr(e) + " - " + str(e)
            print "\tFile: " + str(test) + "_info.csv"
            print "Submitting as an Incomplete Run"
            # Mark this as an incomplete run
            envdict["run_success"] = 2     
        else:
            csvfile.close()
            # Insert memtest info
            table = test + "_info"
            cur = conn.cursor()
            cols = memdict.keys()
            for k, v in memdict.iteritems():
                memdict[k] = conn.escape(v)
            vals = memdict.values()
            try:
                sql = "INSERT INTO %s(%s) VALUES(%s);" % (table, ",".join(cols), ",".join(vals))
                cur.execute(sql)
            except Exception as e:
                print "In insertResults (mem info execute): " + repr(e) + " - " + str(e)
                print "\tTest: " + str(test)
                print "Submitting as an Incomplete Run"
                # Mark this as an incomplete run
                envdict["run_success"] = 2 
        
        # Insert memtest results files
        # Different results file for each socket, so look for all of them
        filelist = []
        for results_file in os.listdir(results_dir):
            if fnmatch.fnmatch(results_file, test + '_out_*'):
                filelist.append(results_file)
        
        for results_file in filelist:
            # Open file, save to dict
            memdict = {}
            path = results_dir + "/" + results_file
            try:
                with open(path) as csvfile:
                    reader = csv.DictReader(csvfile)
                    memdict = list(reader)[0]
            except Exception as e:
                print "In insertResults (mem tests open): " + repr(e) + " - " + str(e)
                print "\tFile: " + str(results_file)
                print "Submitting as an Incomplete Run"
                # Mark this as an incomplete run
                envdict["run_success"] = 2 
            else:
                csvfile.close()
                table = "mem_results"
                cur = conn.cursor()
                cols = memdict.keys()
                vals = memdict.values()
                tests_keys = [x for x in cols if "_max" in x]
                for n, key in enumerate(tests_keys):
                    tests_keys[n] = key.replace("_max", "")
                for key in tests_keys:
                    # For each test name, set up new dict with test results
                    if "_omp" in key:
                        memtestdict = dict((k,v) for k, v in memdict.iteritems() if key in k)
                        memtestdict["nthreads_used"] = memdict["omp_nthreads_used"]
                    elif "_omp" not in key:
                        memtestdict = dict((k,v) for k, v in memdict.iteritems() if key in k and not "_omp" in k)
                        memtestdict["nthreads_used"] = "1"
                    # Populate new dict with other necessary information
                    memtestdict.update({"testname":key, "run_uuid":memdict["run_uuid"], \
                                        "timestamp":memdict["timestamp"], "nodeid":memdict["nodeid"], \
                                        "nodeuuid":memdict["nodeuuid"], "units":memdict["units"], \
                                        "socket_num":memdict["socket_num"], "dvfs":memdict["dvfs"]})

                    # Escape values, setup SQL command and execute
                    cols = memtestdict.keys()
                    for n, k in enumerate(cols):
                        cols[n] = k.replace(key + "_", "")
                    for k, v in memtestdict.iteritems():
                        memtestdict[k] = conn.escape(v)
                    vals = memtestdict.values()
                    try:
                        sql = "INSERT INTO %s(%s) VALUES(%s);" % (table, ",".join(cols), ",".join(vals))
                        cur.execute(sql)
                    except Exception as e:
                        print "In insertResults (mem tests execute): " + repr(e) + " - " + str(e)
                        print "\tFile: " + str(results_file)
                        print "Submitting as an Incomplete Run"
                        # Mark this as an incomplete run
                        envdict["run_success"] = 2 
        
    ###########
    ### FIO ###
    ###########
    #filelist = []
    #for results_file in os.listdir(results_dir):
    #    if fnmatch.fnmatch(results_file, 'fio_*'):
    #        filelist.append(results_file)
    #for results_file in filelist:
    #    # Open file, save to dict
    #    fiodict = {}
    #    path = results_dir + "/" + results_file
    #    try:
    #        with open(path) as csvfile:
    #            reader = csv.DictReader(csvfile)
    #            fiodict = list(reader)[0]
    #    except Exception as e:
    #        print "In insertResults (fio files open): " + repr(e) + " - " + str(e)
    #        print "\tFile: " + str(results_file)
    #        print "Submitting as an Incomplete Run"
    #        # Mark this as an incomplete run
    #        envdict["run_success"] = 2 
    #    else:
    #        csvfile.close()
    #        if "info" in results_file:
    #            table = "fio_info"
    #            cur = conn.cursor()
    #            cols = fiodict.keys()
    #            for k, v in fiodict.iteritems():
    #                fiodict[k] = conn.escape(v)
    #            vals = fiodict.values()
    #
    #        else:
    #            table = "disk_results"
    #            fiotestdict = {"testname":fiodict["jobname"]}
    #            if "read" in results_file:
    #                for key, value in fiodict.iteritems():
    #                    if "READ_bw" in key and "agg" not in key:
    #                        fiotestdict[key.strip("READ_bw_")] = value
    #                fiotestdict.update({"runtime":float(fiodict["READ_runtime"])/1000, "size":fiodict["READ_kb"]})
    #            elif "write" in results_file:
    #                for key, value in fiodict.iteritems():
    #                    if "WRITE_bw" in key and "agg" not in key:
    #                        fiotestdict[key.strip("WRITE_bw_")] = value
    #                fiotestdict.update({"runtime":float(fiodict["WRITE_runtime"])/1000, "size":fiodict["WRITE_kb"]})
    #            fiotestdict['stdev'] = fiotestdict.pop('dev')
    #            fiotestdict.update({"device":fiodict["device"], "units":"KB/s", "iodepth":fiodict["iod"]})
    #
    #            cur = conn.cursor()
    #            fiotestdict.update({"run_uuid":run_uuid, "timestamp":timestamp, \
    #                                "nodeid":nodeid, "nodeuuid":nodeuuid})
    #            for k, v in fiotestdict.iteritems():
    #                fiotestdict[k] = conn.escape(v)
    #            cols = fiotestdict.keys()
    #            vals = fiotestdict.values()          
    #        
    #        try:
    #            sql = "INSERT INTO %s(%s) VALUES(%s);" % (table, ",".join(cols), ",".join(vals))
    #            cur.execute(sql)
    #        except Exception as e:
    #            print "In insertResults (fio files execute): " + repr(e) + " - " + str(e)
    #            print "\tFile: " + str(results_file)
    #            print "Submitting as an Incomplete Run"
    #            # Mark this as an incomplete run
    #            envdict["run_success"] = 2            
    
    ###########
    ### CPU ###
    ###########
    
    ### Regular results ###
    filelist = []
    for results_file in os.listdir(results_dir):
        if fnmatch.fnmatch(results_file, 'npb*.csv'):
            filelist.append(results_file)
    for results_file in filelist:
        # Open file, save to dict
        cpudict = {}
        path = results_dir + "/" + results_file
        try:
            with open(path) as csvfile:
                reader = csv.DictReader(csvfile)
                cpudict = list(reader)[0]
        except Exception as e:
            print "In insertResults (cpu files open): " + repr(e) + " - " + str(e)
            print "\tFile: " + str(results_file)
            print "Submitting as an Incomplete Run"
            # Mark this as an incomplete run
            envdict["run_success"] = 2 
        else:
            csvfile.close()   
            table = "npb_cpu_results"
            for k, v in cpudict.iteritems():
                cpudict[k] = conn.escape(v)
            cols = cpudict.keys()
            vals = cpudict.values()          
            try:
                sql = "INSERT INTO %s(%s) VALUES(%s);" % (table, ",".join(cols), ",".join(vals))
                cur.execute(sql)
            except Exception as e:
                print "In insertResults (cpu files execute): " + repr(e) + " - " + str(e)
                print "\tFile: " + str(results_file)
                print "Submitting as an Incomplete Run"
                # Mark this as an incomplete run
                envdict["run_success"] = 2
        
    ### Extra results ###
    #filelist = []
    #for results_file in os.listdir(results_dir):
    #    if fnmatch.fnmatch(results_file, 'extra-npb*.csv'):
    #        filelist.append(results_file)
    #for results_file in filelist:
    #    # Open file, save to dict
    #    extracpudict = {}
    #    path = results_dir + "/" + results_file
    #    try:
    #        with open(path) as csvfile:
    #            reader = csv.DictReader(csvfile)
    #            extracpudict = list(reader)[0]
    #    except Exception as e:
    #        print "In insertResults (extra cpu files open): " + repr(e) + " - " + str(e)
    #        print "\tFile: " + str(results_file)
    #        print "Submitting as an Incomplete Run"
    #        # Mark this as an incomplete run
    #        envdict["run_success"] = 2 
    #    else:
    #        csvfile.close()
    #        table = "extra_npb_cpu_results"
    #        for k, v in extracpudict.iteritems():
    #            extracpudict[k] = conn.escape(v)
    #        cols = extracpudict.keys()
    #        vals = extracpudict.values()             
    #        try:
    #            sql = "INSERT INTO %s(%s) VALUES(%s);" % (table, ",".join(cols), ",".join(vals))
    #            cur.execute(sql)
    #        except Exception as e:
    #            print "In insertResults (extra cpu files execute): " + repr(e) + " - " + str(e)
    #            print "\tFile: " + str(results_file)
    #            print "Submitting as an Incomplete Run"
    #            # Mark this as an incomplete run
    #            envdict["run_success"] = 2
    
    #####################
    ### NETWORK TESTS ###
    #####################

    ### Ping results ###
    # Open file, save to dict
    #pingtestdict = {}
    #results_file = "ping_results.csv"
    #path = results_dir + "/" + results_file
    #try:
    #    with open(path) as csvfile:
    #        reader = csv.DictReader(csvfile)
    #        pingtestdict = list(reader)[0]
    #except Exception as e:
    #    print "In insertResults (ping results open): " + repr(e) + " - " + str(e)
    #    print "\tFile: " + str(results_file)
    #    print "Submitting as an Incomplete Run"
    #    # Mark this as an incomplete run
    #    envdict["run_success"] = 2
    #else:
    #    csvfile.close()
    #    # Set table, escape values, setup SQL command and execute
    #    table = 'ping_results'
    #    for k, v in pingtestdict.iteritems():
    #        pingtestdict[k] = conn.escape(v)
    #    cols = pingtestdict.keys()
    #    vals = pingtestdict.values()
    #    try:
    #        sql = "INSERT INTO %s(%s) VALUES(%s);" % (table, ",".join(cols), ",".join(vals))
    #        cur.execute(sql)
    #    except Exception as e:
    #        print "In insertResults (ping results execute): " + repr(e) + " - " + str(e)
    #        print "Submitting as an Incomplete Run"
    #        # Mark this as an incomplete run
    #        envdict["run_success"] = 2
    #
    ### Ping info ###
    # Open file, save to dict
    #pinginfodict = {}
    #results_file = "ping_info.csv"
    #path = results_dir + "/" + results_file
    #try:
    #    with open(path) as csvfile:
    #        reader = csv.DictReader(csvfile)
    #        pinginfodict = list(reader)[0]
    #except Exception as e:
    #    print "In insertResults (ping info open): " + repr(e) + " - " + str(e)
    #    print "\tFile: " + str(results_file)
    #    print "Submitting as an Incomplete Run"
    #    # Mark this as an incomplete run
    #    envdict["run_success"] = 2
    #else:
    #    csvfile.close()
    #    dest_nodeid = pinginfodict["ping_dest_nodeid"]
    #
    #    # Set table, escape values, setup SQL command and execute
    #    table = 'ping_info'
    #    for k, v in pinginfodict.iteritems():
    #        pinginfodict[k] = conn.escape(v)
    #    cols = pinginfodict.keys()
    #    vals = pinginfodict.values()
    #    try:
    #        sql = "INSERT INTO %s(%s) VALUES(%s);" % (table, ",".join(cols), ",".join(vals))
    #        cur.execute(sql)
    #    except Exception as e:
    #        print "In insertResults (ping info execute): " + repr(e) + " - " + str(e)
    #        print "Submitting as an Incomplete Run"
    #        # Mark this as an incomplete run
    #        envdict["run_success"] = 2
    #
    # The following tests only run on Utah machines and r320s at APT.
    #if (site == 'utah') or (hw_type == 'r320'):
    #    ### Probed info ###
    #    # Open file, save to dict
    #    probedinfodict = {}
    #    results_file = "probed_info.csv"
    #    path = results_dir + "/" + results_file
    #    try:
    #        with open(path) as csvfile:
    #            reader = csv.DictReader(csvfile)
    #            probedinfodict = list(reader)[0]
    #    except Exception as e:
    #        print "In insertResults (probed info open): " + repr(e) + " - " + str(e)
    #        print "\tFile: " + str(results_file)
    #        print "Submitting as an Incomplete Run"
    #        # Mark this as an incomplete run
    #        envdict["run_success"] = 2
    #    else:
    #        csvfile.close()
    #
    #        # Set table, escape values, setup SQL command and execute
    #        table = 'probed_info'
    #        for k, v in probedinfodict.iteritems():
    #            probedinfodict[k] = conn.escape(v)
    #        cols = probedinfodict.keys()
    #        vals = probedinfodict.values()
    #        try:
    #            sql = "INSERT INTO %s(%s) VALUES(%s);" % (table, ",".join(cols), ",".join(vals))
    #            cur.execute(sql)
    #        except Exception as e:
    #            print "In insertResults (probed info execute): " + repr(e) + " - " + str(e)
    #            print "Submitting as an Incomplete Run"
    #            # Mark this as an incomplete run
    #            envdict["run_success"] = 2
    #            
    #    ### Probed results ###
    #    # Open file, save to dict
    #    probedtestdict = {}
    #    results_file = "probed_out.csv"
    #    path = results_dir + "/" + results_file
    #    try:
    #        with open(path) as csvfile:
    #            reader = csv.DictReader(csvfile)
    #            probedtestdict = list(reader)[0]
    #    except Exception as e:
    #        print "In insertResults (probed results open): " + repr(e) + " - " + str(e)
    #        print "\tFile: " + str(results_file)
    #        print "Submitting as an Incomplete Run"
    #        # Mark this as an incomplete run
    #        envdict["run_success"] = 2
    #    else:
    #        csvfile.close()
    #
    #        # Set table, escape values, setup SQL command and execute
    #        table = 'probed_results'
    #        for k, v in probedtestdict.iteritems():
    #            probedtestdict[k] = conn.escape(v)
    #        cols = probedtestdict.keys()
    #        vals = probedtestdict.values()
    #        try:
    #            sql = "INSERT INTO %s(%s) VALUES(%s);" % (table, ",".join(cols), ",".join(vals))
    #            cur.execute(sql)
    #        except Exception as e:
    #            print "In insertResults (probed results execute): " + repr(e) + " - " + str(e)
    #            print "Submitting as an Incomplete Run"
    #            # Mark this as an incomplete run
    #            envdict["run_success"] = 2
    #    
    ### iperf3 info/results ###
    # d710s are having issues with iperf3 and their 1GbE links
    #if hw_type != 'd710':
    #    results_files = ["iperf3_normal.json", "iperf3_reversed.json"]
    #    for results_file in results_files:
    #        path = results_dir + "/" + results_file
    #        try:
    #            with open(path) as jsonfile:
    #                iperfdict = json.load(jsonfile)
    #        except Exception as e:
    #            print "In insertResults (iperf3 open): " + repr(e) + " - " + str(e)
    #            print "\tFile: " + str(results_file)
    #            print "Submitting as an Incomplete Run"
    #            # Mark this as an incomplete run
    #            envdict["run_success"] = 2
    #        else:
    #            jsonfile.close()
    #            try:
    #                start = iperfdict["start"]
    #                end = iperfdict["end"]
    #                cpu_util = end["cpu_utilization_percent"]
    #            except Exception as e:
    #                print "In insertResults (iperf3 results formatting): " + repr(e) + " - " + str(e)
    #                print "\tFile: " + str(results_file)
    #                print "Submitting as an Incomplete Run"
    #                # Mark this as an incomplete run
    #                envdict["run_success"] = 2
    #            else:
    #                if "normal" in results_file:            
    #                    # Just insert iperf3 information once, we'll do it from the normal file
    #                    test_start = start["test_start"]
    #                    connected = start["connected"][0]
    #                    iperfinfodict = {"run_uuid":run_uuid, "timestamp":timestamp, \
    #                                     "nodeid":nodeid, "nodeuuid":nodeuuid, \
    #                                     "version":start["version"], "local_ip":connected["local_host"], \
    #                                     "local_port":connected["local_port"], \
    #                                     "remote_ip":connected["remote_host"], \
    #                                     "remote_port":connected["remote_port"], \
    #                                     "remote_nodeid":dest_nodeid, "protocol":test_start["protocol"], \
    #                                     "num_streams":test_start["num_streams"], "buffer_size":test_start["blksize"], \
    #                                     "omitted_intervals":test_start["omit"], "duration":test_start["duration"], "time_units":"s"}
    #                    table = 'iperf3_info'
    #                    for k, v in iperfinfodict.iteritems():
    #                        iperfinfodict[k] = conn.escape(v)
    #                    cols = iperfinfodict.keys()
    #                    vals = iperfinfodict.values()
    #                    try:
    #                        sql = "INSERT INTO %s(%s) VALUES(%s);" % (table, ",".join(cols), ",".join(vals))
    #                        cur.execute(sql)
    #                    except Exception as e:
    #                        print "In insertResults (iperf3 info execute): " + repr(e) + " - " + str(e)
    #                        print "\tFile: " + str(results_file)
    #                        print "Submitting as an Incomplete Run"
    #                        # Mark this as an incomplete run
    #                        envdict["run_success"] = 2
    #                     
    #                # Experiment stats
    #                iperftestdict = {"run_uuid":run_uuid, "timestamp":timestamp, \
    #                                 "nodeid":nodeid, "nodeuuid":nodeuuid, \
    #                                 "reverse":start["test_start"]["reverse"], "retransmits":end["sum_sent"]["retransmits"], \
    #                                 "local_cpu_util":cpu_util["host_total"], "remote_cpu_util":cpu_util["remote_total"]}
    #
    #                # Bandwidth results
    #                rates = [interval['sum']['bits_per_second'] for interval in iperfdict['intervals'] if interval['sum']['omitted'] == False]
    #                iperftestdict.update({"mean":np.asscalar(np.mean(rates)), "median":np.asscalar(np.median(rates)), \
    #                                      "min":np.asscalar(np.min(rates)), "max":np.asscalar(np.max(rates)), \
    #                                      "stdev":np.asscalar(np.std(rates)), "sum_sent":end["sum_sent"]["bits_per_second"], \
    #                                      "sum_received":end["sum_received"]["bits_per_second"], "units":"bps"})
    #                table = 'iperf3_results'
    #                for k, v in iperftestdict.iteritems():
    #                    iperftestdict[k] = conn.escape(v)
    #                cols = iperftestdict.keys()
    #                vals = iperftestdict.values()
    #                try:
    #                    sql = "INSERT INTO %s(%s) VALUES(%s);" % (table, ",".join(cols), ",".join(vals))
    #                    cur.execute(sql)
    #                except Exception as e:
    #                    print "In insertResults (iperf3 results execute): " + repr(e) + " - " + str(e)
    #                    print "\tFile: " + str(results_file)
    #                    print "Submitting as an Incomplete Run"
    #                    # Mark this as an incomplete run
    #                    envdict["run_success"] = 2
        

    ####################
    ### NETWORK INFO ###
    ####################
    
    # Open file, save to dict
    netdict = {}
    results_file = "net_info.csv"
    path = results_dir + "/" + results_file
    try:
        with open(path) as csvfile:
            reader = csv.DictReader(csvfile)
            netdict = list(reader)[0]
    except Exception as e:
        print "In insertResults (net info open): " + repr(e) + " - " + str(e)
        print "\tFile: " + str(results_file)
        print "Submitting as an Incomplete Run"
        # Mark this as an incomplete run
        envdict["run_success"] = 2
    else:
        csvfile.close()  
        # Placeholder for when we eventually get switch paths from the manifest
        netdict["switch_path"] = switchpath
        # Set table, escape values, setup SQL command and execute
        table = 'network_info'
        for k, v in netdict.iteritems():
            netdict[k] = conn.escape(v)
        cols = netdict.keys()
        vals = netdict.values()
        try:
            sql = "INSERT INTO %s(%s) VALUES(%s);" % (table, ",".join(cols), ",".join(vals))
            cur.execute(sql)
        except Exception as e:
            print "In insertResults (net info execute): " + repr(e) + " - " + str(e)
            print "Submitting as an Incomplete Run"
            # Mark this as an incomplete run
            envdict["run_success"] = 2

    #######################
    ### ENV (CONTINUED) ###
    #######################
    table = 'env_info'
    for k, v in envdict.iteritems():
        envdict[k] = conn.escape(v)
    cols = envdict.keys()
    vals = envdict.values()
    try:
        sql = "INSERT INTO %s(%s) VALUES(%s);" % (table, ",".join(cols), ",".join(vals))
        cur.execute(sql)
    except Exception as e:
        # This is the one insertion we want to treat as non-negotiable.
        # If this fails, the run should be listed as failed.
        print "In insertResults (env info execute): " + repr(e) + " - " + str(e)
        cur.close()
        conn.close()
        insertFailure(nodename, site)
        return "Failure"
    
    #########################        
    ### COMMIT EXECUTIONS ###
    ######################### 
    # Only commit if every execution went through okay.
    try:
        conn.commit()
    except Exception as e:
        print "In insertResults (commit): " + repr(e) + " - " + str(e)
        return "Failure"
    else:
        print "SQL Insert success."
    finally:
        # Close SQL connection
        cur.close()
        conn.close()
    return "Success"

##############################################
### Insert a failed experiment run into DB ###
##############################################
def insertFailure(nodename, site):
    print "Inserting Failure state into SQL database"

    # Set up SQL connection vars
    server = 'localhost'
    user = 'tbbench_user'
    password = 'tbbench_pass'
    database = 'tbbench'
    
    # Try to connect to the DB.  Give it a few tries before failing
    nTries = 0
    maxTries = 3
    while True:
        try:
            conn = pymysql.connect(host = server, user = user, passwd = password, db = database)
        except Exception as e:
            nTries += 1
            print "In insertResults: " + repr(e) + " - " + str(e)
            print "Error #" + str(nTries) + " out of " + str(maxTries) + "."
            if nTries >= maxTries:
                print "\tCould not connect to DB, exiting..."
                sys.exit(2)
            else:
                sleep(10)
                print "\tRetrying..."
        else:
            cur = conn.cursor()
            break
    currenttime = int(time())
    faildict = {"nodeid":nodename, "site":site, "timestamp":currenttime, "run_success":0}
    
    table = 'env_info'
    for k, v in faildict.iteritems():
        faildict[k] = conn.escape(v)
    cols = faildict.keys()
    vals = faildict.values()
    sql = "INSERT INTO %s(%s) VALUES(%s);" % (table, ",".join(cols), ",".join(vals))
    try:
        cur.execute(sql)
        conn.commit()
    except Exception as e:
        print "In insertResults: " + repr(e) + " - " + str(e)
        print "Unable to insert failure into SQL database"
    finally:
        cur.close()
        conn.close()


#####################
### Main function ###
#####################
def main():    
    # Parse and set up args
    args = parseArgs()
    nslices = args.nslices
    site = args.site
    nodes_d6515 = []

    # Info for log
    print "Starting benchmarking at testbed site " + site + " - " + str(datetime.datetime.today())

    # Optional:  Remove cached files
    if args.geni_cache_path != "" and os.path.exists(args.geni_cache_path):
        files = os.listdir(args.geni_cache_path)
        for file in files:
            os.remove(os.path.join(args.geni_cache_path,file))

    # Before attempting to contact AM at all, attempt to adjust timeout
    geniconf.HTTP.TIMEOUT = 60

    # Set up context, get list of free m510 nodes
    print "Building Context, fetching list of free nodes."
    context = buildContext(args.cert_path, args.pub_key_path, args.user, args.project)
    nodes = getFreeNodes(context, site)
    if site == "utah":
        nodes_d6515 = getFreeNodes(context, "utah_d6515")
    
    # Check for number of free nodes.  Exit out early if there are none.
    if (len(nodes) + len(nodes_d6515)) == 0:
        print "\tNo nodes available to run experiments on.  Exiting."
        exit(2)
    elif (len(nodes) + len(nodes_d6515)) < nslices:
        nslices = (len(nodes) + len(nodes_d6515))
    # Get unique hardware types from free nodes
    hwtypes = []
    for node in nodes:
        if node.hw_type not in hwtypes:
            hwtypes.append(node.hw_type)

    # Set up node candidates and slices
    if len(nodes_d6515) >= nslices:
        testnodes = chooseTestNodes(context, nodes_d6515, nslices, site, ['d6515'])
    elif (len(nodes_d6515) < nslices) and (len(nodes_d6515) > 0):
        testnodes_d6515 = chooseTestNodes(context, nodes_d6515, len(nodes_d6515), site, ['d6515'])
        testnodes_other = chooseTestNodes(context, nodes, (nslices-len(nodes_d6515)), site, hwtypes)
        testnodes = testnodes_d6515 + testnodes_other
    else:
        testnodes = chooseTestNodes(context, nodes, nslices, site, hwtypes)
    # Probably redundant, but I don't want to take any chances
    if len(testnodes) == 0:
        print "\tNo nodes available to run experiments on.  Exiting."
        exit(2)
    elif len(testnodes) < nslices:
        nslices = len(testnodes)
    slices = {}
    # Set slice lifetimes in minutes based on number of sockets and number of disks 
    # for the longest hardware type at each site.
    #lifetimes = {"utah": 140, "wisc": 340, "clemson": 240, "apt": 240, "emulab": 340}
    # Make slice lifetimes homogeneous.  Since it's a short invocation, use 2h40m.
    lifetimes = {"utah": 160, "wisc": 160, "clemson": 160, "apt": 160, "emulab": 160}
    lifetime = lifetimes[site]
    sliceexp = datetime.datetime.utcnow() + datetime.timedelta(minutes = lifetime)
    for n in range(0, nslices):
        slicename = "bench-" + site + "-" + str(n)
        testnode = testnodes[n]
        result = createSlice(context, slicename, sliceexp)
        if result == 0:
            print "Node " + str(n) + " chosen and slice created - requesting Node " + testnode[0] + " in Slice " + slicename + "."
            # Sleep for 1 minute to stagger sliver creation (test)
            sleep(60)
            result = createSliver(context, testnode, slicename, nodes, hwtypes, site, n+1)
            if result != "IGNORESLICE":
                testnode = result
                slices[slicename] = testnode
                print "Node " + testnode[0] + " successfully requested for slice " + slicename + "."

    print str(len(slices)) + " out of " + str(nslices) + " requested slices successfully created."

    # Exit here if no slices have been created.
    if len(slices) == 0:
        print "\tNo slices available to run experiments on.  Exiting."
        exit(2)
 
    portnum = 22
    threads = [None] * len(slices)
    thread_results = [None] * len(slices)
    for n, item in enumerate(slices.items()):
        slicename = item[0]
        nodename = item[1][0]
        threadname = nodename + "-thread"
        if site in ['utah', 'wisc', 'clemson']:
            server = nodename + "." + site + ".cloudlab.us"
        elif site == 'apt':
            server = nodename + "." + site + ".emulab.net"
        elif site == 'emulab':
            server = nodename + "." + site + ".net"
        dest_dir = args.dest_dir + "/" + nodename
        threads[n] = sshThread(n, threadname, server, portnum, args.user, args.priv_key_path, dest_dir, sliceexp, thread_results)
        threads[n].start()
      
    # Get the PG URL  
    sleep(480)
    for n, item in enumerate(slices.items()):
        slicename = item[0]
        nodename = item[1][0]
        try:
            if site == "utah":
                res = cloudlab.Utah.sliverstatus(context, slicename)
            elif site == "wisc":
                res = cloudlab.Wisconsin.sliverstatus(context, slicename)
            elif site == "clemson":
                res = cloudlab.Clemson.sliverstatus(context, slicename)
            elif site == "apt":
                res = apt.Apt.sliverstatus(context, slicename)
            elif site == "emulab":
                res = emulabPG.UTAH_PG.sliverstatus(context, slicename)
            print "\tSliver URL (" + str(nodename) + "): " + str(res['pg_public_url'])
        except Exception as e:
            print "In main: " + repr(e) + " - " + str(e)
            print "Unable to get sliver URL"
            
    # Wait for threads to finish
    for t in threads:
        t.join()

    # Might have an issue with the credential expiration at this point
    # We'll delete the old files and re-build the context
    # Optional:  Remove cached files
    if args.geni_cache_path != "" and os.path.exists(args.geni_cache_path):
        files = os.listdir(args.geni_cache_path)
        for file in files:
            os.remove(os.path.join(args.geni_cache_path,file))
    context = buildContext(args.cert_path, args.pub_key_path, args.user, args.project)

    # Delete slivers, insert results into DB
    nSuccesses = 0
    for n, item in enumerate(slices.items()):
        slicename = item[0]
        nodename = item[1][0]
        hw_type = item[1][1]
        dest_dir = args.dest_dir + "/" + nodename
        if thread_results[n] == "Success":
            switchpath = getSwitchPath(context, slicename, site)
            result = insertResults(dest_dir, hw_type, site, switchpath, nodename)
            if result == "Success":
                nSuccesses += 1
        else:
            insertFailure(nodename, site)
        
        # Move deleteSliver to the end so we can get switchpath info
        deleteSliver(context, slicename, site)
        
    print str(nSuccesses) + " out of " + str(nslices) + " experiments successfully run and stored."

##################################################
### Entry point to program, call main function ###
##################################################
if __name__ == "__main__":
    main()
else:
    print "Error, cannot enter main, exiting."
    sys.exit(2)
